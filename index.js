
// Object.prototype[Symbol.toPrimitive] = function(hint) {
//     console.log(`Происходит преобразование: ${hint}`)
//     if (hint == 'string') {
//         return this.toString();
//     } else {
//         let res = this.valueOf();
//         if (res === this) {
//             return res.toString();
//         } else {
//            return res;
//         }
//     }
//     // return hint == 'string' ? this.toString().valueOf() : (this.valueOf() != this ? this.valueOf().toString() : this.valueOf())
// }


// let a = {
//     number: 3,
//     valueOf() {
//         return this.number + 100
//     }
// }


// console.log('SECOND',[2] +  +a)


// let str = 'str'
// console.log(typeof str.valueOf())

function isFunction(func) {
    return Object.prototype.toString.call(func) === "[object Function]";
}

Object.prototype[Symbol.toPrimitive] = function(hint) {
    console.log(`Происходит преобразование: ${hint}`);
    let tempValue = 0;
    let valOf = '';
    if (hint === 'string') {
        if (isFunction(this)) {
            tempValue = this.toString();
        } else {
            return this.toString();
        }
        valOf = tempValue.valueOf();
        if (isFunction(valOf)) {
            tempValue = valOf;
        } else {
            return valOf;
        }
    } else {
        valOf = this.valueOf();
        if (!isFunction(valOf)) {
            return valOf.toString();
        }
    }
    throw new TypeError();
};


// Object.prototype[Symbol.toPrimitive] = function(hint) {
//     console.log(`Происходит преобразование: ${hint}`);
//         let tempValue = 0;
//         let valOf = '';
//         let toStr = '';
//         if (hint === 'string') {
//             if (isFunction(this)) {
//                 tempValue = this.toString();
//             } else {
//                 return this.toString();
//             }
//             valOf = tempValue.valueOf();
//             if (isFunction(valOf)) {
//                 tempValue = valOf;
//             } else {
//                 return valOf;
//             }
//         } else if (hint === 'number') {
//             valOf = this.valueOf();
//             if (isFunction(valOf)) {
//                 tempValue = valOf;
//             } else  {
//                 console.log('RETURN VALUE OF DEFAULT', typeof valOf);
//                 return valOf;
//             }
//             if(isFunction(tempValue.toString())) {
//                 tempValue = tempValue.toString();
//             } else {
//                 return tempValue.toString();
//             }
//         } else {
//             if (typeof this.valueOf() !== this) {
//                 return this.valueOf().toString()
//             }
//         }
//     throw new TypeError();
// };


